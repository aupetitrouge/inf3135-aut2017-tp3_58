#include "map.h"

// --------------- //
// Data structures //
// --------------- //

void Map_generateTexture(struct Map *map);

static SDL_Renderer *Map_renderer;

void *Map_imageLoader(const char *path)
{
		return IMG_LoadTexture(Map_renderer, path);
}

// ---------------- //
// Public functions //
// ---------------- //

struct Map *Map_create(char *filename, SDL_Renderer *renderer)
{
	Map_renderer = renderer;
	SDL_Surface* image = IMG_Load(filename);
	if (image == NULL) {
		fprintf(stderr, "CreateRGBSurface failed: %s\n", SDL_GetError());
		exit(1);
	} else {
		struct Map *map;
		map = (struct Map*)malloc(sizeof(struct Map));
		map->image = image;
		map->renderer = renderer;
		Map_generateTexture(map);
		SDL_FreeSurface(image);
		Map_wallCreate(map);
		return map;
	}

}

void Map_render(struct Map *map)
{
	SDL_RenderCopy(map->renderer, map->texture, NULL, NULL);
	//SDL_RenderDrawRects(map->renderer, map->coordPoints, 11);
}


void Map_delete(struct Map *map) {
	if (map != NULL) {
		if (map->texture != NULL) {
			SDL_DestroyTexture(map->texture);
			map->texture = NULL;
		}
		free(map);
	}
}

// ----------------- //
// Private functions //
// ----------------- //


void Map_generateTexture(struct Map *map)
{
	map->texture = SDL_CreateTextureFromSurface(map->renderer, map->image);
	if (map->texture == NULL) {
		fprintf(stderr, "CreateTextureFromSurface failed: %s\n", SDL_GetError());
		exit(1);
	}
	SDL_SetRenderTarget(map->renderer, map->texture);
}

void Map_wallCreate(struct Map *map)
{
	// Wall-0
	map->coordPoints[0].x = 149;
	map->coordPoints[0].y = 580;
	map->coordPoints[0].w = 685;
	map->coordPoints[0].h = 30;
	// Wall-1
	map->coordPoints[1].x = 0;
	map->coordPoints[1].y = 460;
	map->coordPoints[1].w = 169;
	map->coordPoints[1].h = 30;
	// Wall-2
	map->coordPoints[2].x = 0;
	map->coordPoints[2].y = 220;
	map->coordPoints[2].w = 169;
	map->coordPoints[2].h = 30;
	// Wall-3
	map->coordPoints[3].x = 149;
	map->coordPoints[3].y = 340;
	map->coordPoints[3].w = 478;
	map->coordPoints[3].h = 30;
	// Wall-4
	map->coordPoints[4].x = 319;
	map->coordPoints[4].y = 370;
	map->coordPoints[4].w = 30;
	map->coordPoints[4].h = 330;
	// Wall-5
	map->coordPoints[5].x = 319;
	map->coordPoints[5].y = 20;
	map->coordPoints[5].w = 30;
	map->coordPoints[5].h = 230;
	// Wall-6
	map->coordPoints[6].x = 319;
	map->coordPoints[6].y = 20;
	map->coordPoints[6].w = 30;
	map->coordPoints[6].h = 230;

	// Wall-7
	map->coordPoints[7].x = 349;
	map->coordPoints[7].y = 220;
	map->coordPoints[7].w = 485;
	map->coordPoints[7].h = 30;
	// Wall-8
	map->coordPoints[8].x = 499;
	map->coordPoints[8].y = 100;
	map->coordPoints[8].w = 485;
	map->coordPoints[8].h = 30;
	// Wall-9
	map->coordPoints[9].x = 778;
	map->coordPoints[9].y = 340;
	map->coordPoints[9].w = 206;
	map->coordPoints[9].h = 30;
	// Wall-10
	map->coordPoints[10].x = 499;
	map->coordPoints[10].y = 460;
	map->coordPoints[10].w = 485;
	map->coordPoints[10].h = 30;
}
